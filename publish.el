(require 'ox-publish)

(defvar site-attachments
  (regexp-opt '("jpg" "jpeg" "gif" "png" "svg"
		"ico" "js"  "html" "pdf"))
  "File types that are published as static files.")

(defvar en-header
   "<div class=\"cabecalho\">
     <h1>Magali's Blog</h1>
     <div class=\"dropdown\">
      <div class=\"dropbtn\">EN</div>
      <div class=\"dropdown-content\">
       <a href='/pt/blog/'>PT</a>
      </div>
    </div>
    </div>
    <div id=\"menu\">
     <ul>
      <li><a href='/blog'>Home</a></li>
      <li><a href='/about'>About</a></li>
     </ul>
    </div>")

(defvar pt-header
  "<div class=\"cabecalho\">
    <h1>Blog da Magali</h1>
   </div>
   <div id=\"menu\">
    <ul>
     <li><a href='/pt/blog'>Início</a></li>
     <li><a href='/pt/about'>About</a></li>
    </ul>
   </div>")

(setq org-publish-project-alist
      `(("posts"
	 :base-directory "blog/posts/"
	 :base-extension "org"
	 :publishing-directory "public/blog"
	 :recursive t
	 :auto-sitemap t
	 :sitemap-title "Posts"
	 :sitemap-filename "index.org"
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 :html-head "<link rel=\"stylesheet\" href=\"../css/site.css\" type=\"text/css\"/>"
	 :author "Magali Lemes"
	 :with-creator t
	 :html-preamble ,en-header
	 :publishing-function org-html-publish-to-html)
	("posts-pt"
	 :base-directory "pt/blog/posts/"
	 :base-extension "org"
	 :publishing-directory "public/pt/blog"
	 :recursive t
	 :auto-sitemap t
	 :sitemap-title "Postagens"
	 :sitemap-filename "index.org"
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 :html-head "<link rel=\"stylesheet\" href=\"../../css/site.css\" type=\"text/css\"/>"
	 :author "Magali Lemes"
	 :with-creator t
	 :html-preamble ,pt-header
	 :publishing-function org-html-publish-to-html)
	("about"
	 :base-directory "about"
	 :base-extension "org"
	 :publishing-directory "public/about"
	 :recursive t
	 :html-head "<link rel=\"stylesheet\" href=\"../css/site.css\" type=\"text/css\"/>"
	 :author "Magali Lemes"
	 :with-creator t
	 :html-preamble ,en-header
	 :publishing-function org-html-publish-to-html)
	("about-pt"
	 :base-directory "pt/about"
	 :base-extension "org"
	 :publishing-directory "public/pt/about"
	 :recursive t
	 :html-head "<link rel=\"stylesheet\" href=\"../../css/site.css\" type=\"text/css\"/>"
	 :author "Magali Lemes"
	 :with-creator t
	 :html-preamble ,pt-header
	 :publishing-function org-html-publish-to-html)
	("css"
	 :base-directory "css/"
	 :base-extension "css"
	 :publishing-directory "public/css/"
	 :publishing-function org-publish-attachment
	 :recursive t)
	("site-static"
	 :base-directory "media"
	 :exclude "public/"
	 :base-extension ,site-attachments
	 :publishing-directory "public/media"
	 :publishing-function org-publish-attachment
	 :recursive t)
	("all" :components ("posts" "css"))))
